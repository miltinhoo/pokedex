# Pokedex

Es neceario realizar un npm install antes de correr el proyecto
Este proyecto se puede correr con el comando ng serve

Los datos de sesión del usuario se manejan haciendo uso del patrón de diseño Redux, pero además estos datos se persisten en el navegador con el fin de brindar una buena experiencia al usuario

# Authentication
* Login: es necesario ingresar un email válido, y la contraseña 123
* Register: Es necesario ingresar un nombre, un email válido, una contraseña válida (con almenos 8 caracteres, dos letras mayusculas, un número y un caracter especial)
* Register: Si se quiere hacer fallar el formulario basta con escribir en minuscula la palabra error en el nombre y escribir todos los otros datos de forma correcta

# Librerias utilizadas
* Se utiliza ngrx para implementar redux en el proyecto
* Se utiliza la libreria ngrx-store-localstorage para persistir los datos de sesión utilizando ngrx
* Se utiliza angular material
* Se utiliza la librería ngx-skeleton-loader
