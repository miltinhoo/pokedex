import { HttpClientModule } from '@angular/common/http';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { StoreModule, ActionReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { localStorageSync } from 'ngrx-store-localstorage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { appReducers } from './app.reducer';
import { clearState } from './shared/store/clear/clear-state.function';
import { effectsArr } from './app.effect';
import { storeDevToolsConfig } from './redux.configuration';


export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({keys: ['sessionState', 'selectedPokemonState'], rehydrate: true })(reducer);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot(appReducers, { metaReducers: [clearState, localStorageSyncReducer] }),
    EffectsModule.forRoot(effectsArr),
    ...storeDevToolsConfig(environment.production)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
