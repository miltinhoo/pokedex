import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuardService } from './shared/guards/authentication-guard/authentication-guard.service';

const routes: Routes = [
  {
    path: 'content',
    loadChildren: () => import('./content/content.module').then(childModule => childModule.ContentModule),
    canActivate: [AuthenticationGuardService]
  },
  {
    path: 'authentication',
    loadChildren: () => import('./authentication/authentication.module').then(childModule => childModule.AuthenticationModule)
  },
  { path: '',
    redirectTo: 'content',
    pathMatch: 'full'
  },
  { path: '**',  redirectTo: 'content' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
