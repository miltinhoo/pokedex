import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { User } from 'src/app/shared/model/user';
import { LoginUserSuccess } from 'src/app/shared/store/session/actions/session.actions';
import { AppState } from './../../app.reducer';
import { AlertMessagesService } from './../../shared/alert-messages/alert-messages.service';
import { FormValidationService } from './../../shared/form-validation/form-validation.service';
import { ErrorModel } from './../../shared/model/error';
import { routesApp } from './../../shared/routes/routes';
import { UserDataFormComponent } from './../../shared/user-data-form/user-data-form.component';
import { RegisterService } from './register-service/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends UserDataFormComponent implements OnInit, OnDestroy {

  form: FormGroup;
  loginLink = routesApp.session.login;

  constructor(
    protected fb: FormBuilder,
    protected formValidationService: FormValidationService,
    protected router: Router,
    protected registerService: RegisterService,
    protected alertMessagesService: AlertMessagesService,
    private store: Store<AppState>
  ) {
    super(fb, formValidationService, alertMessagesService, registerService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  createForm(): void {
    this.form = this.fb.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      passwordRepeat: ['', [Validators.required]]
    });
  }

  saveUser(user: User): void {
    this.store.dispatch(new LoginUserSuccess(user));
    this.router.navigate([routesApp.content.dashboard]);
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.registerSubscription$ = this.registerService.registerUser(
        this.form.controls.fullName.value,
        this.form.controls.email.value,
        this.form.controls.password.value
      ).pipe(
        tap((user: User) => this.store.dispatch(new LoginUserSuccess(user))),
        tap((user: User) => this.router.navigate([routesApp.content.dashboard])),
        catchError((error: ErrorModel) => of(this.alertMessagesService.showErrorMessage(error)))
      ).subscribe();
    }
  }

}
