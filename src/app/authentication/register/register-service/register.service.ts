import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { registerUserErrorMock } from 'src/app/shared/mock/response-services.mock';
import { User } from 'src/app/shared/model/user';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor() { }

  registerUser(name: string, mail: string, password: string): Observable<User> {
    return name === 'error' ? throwError(registerUserErrorMock) :  of({name, mail, password});
  }

  updateUser(name: string, mail: string, password: string): Observable<User> {
    return this.registerUser(name, mail, password);
  }
}
