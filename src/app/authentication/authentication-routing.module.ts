import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routerPath } from '../shared/routes/routes';


const routes: Routes = [
    {
      path: routerPath.session.login,
      loadChildren: () => import('./login/login.module').then(childModule => childModule.LoginModule)
    },
    {
      path: routerPath.session.register,
      loadChildren: () => import('./register/register.module').then(childModule => childModule.RegisterModule)
    },
    { path: '',
      redirectTo: routerPath.session.login,
      pathMatch: 'full'
    },
    { path: '**',  redirectTo: routerPath.session.login }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
