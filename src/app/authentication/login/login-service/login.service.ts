import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { userLoginMock, userLoginFailMock } from 'src/app/shared/mock/response-services.mock';
import { User } from 'src/app/shared/model/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  loginUser(email: string, password: string): Observable<User> {
    return password === '123' ? of(userLoginMock) : throwError(userLoginFailMock);
  }
}
