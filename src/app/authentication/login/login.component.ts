import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { AppState } from 'src/app/app.reducer';
import { routesApp } from 'src/app/shared/routes/routes';
import { LoginUser } from '../../shared/store/session/actions/session.actions';
import { SessionState } from '../../shared/store/session/reducers/session.reducer';
import { AlertMessagesService } from './../../shared/alert-messages/alert-messages.service';
import { invalidEmailMessage } from './../../shared/constants/text-form.constant';
import { FormValidationService } from './../../shared/form-validation/form-validation.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;
  registerLink = routesApp.session.register;

  invalidEmail = invalidEmailMessage;

  loginSubscription$ = new Subscription();

  constructor(
    private fb: FormBuilder,
    private formValidationService: FormValidationService,
    private store: Store<AppState>,
    private router: Router,
    private alertMessagesService: AlertMessagesService
  ) { }

  ngOnInit() {
    this.createForm();
    this.listenValidateUser();
  }

  ngOnDestroy() {
    this.loginSubscription$.unsubscribe();
  }

  createForm(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.store.dispatch(new LoginUser(this.form.controls.email.value, this.form.controls.password.value));
    }
  }

  addValidationEmail(): void {
    this.form.controls.email.setValidators([this.formValidationService.validateEmail()]);
    this.form.controls.email.updateValueAndValidity();
  }

  listenValidateUser(): void {
    this.loginSubscription$ = this.store.select(state => state.sessionState).pipe(
      tap((sessionState: SessionState) => {
        if (sessionState.error) {
          this.alertMessagesService.showErrorMessage(sessionState.error);
        }
      }),
      filter((sessionState: SessionState) => sessionState.isLogin && !sessionState.error),
      tap(() => this.router.navigate([routesApp.content.dashboard]))
    ).subscribe();
  }

}
