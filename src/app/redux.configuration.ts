import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ModuleWithProviders } from '@angular/core';

export function storeDevToolsConfig(isProd: boolean): ModuleWithProviders[] {
    return isProd ? [] : [
        StoreDevtoolsModule.instrument({
          maxAge: 25, // Retains last 25 states
          logOnly: isProd, // Restrict extension to log-only mode
        })
      ];
}
