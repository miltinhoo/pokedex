import { MatIconModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { ContentRoutingModule } from './content-routing.module';
import { ContentComponent } from './content.component';
import { HeaderComponent } from './header/header.component';
import { selectedPokemonReducer } from './store/reducers/selected-pokemon.reducer';

@NgModule({
  declarations: [ContentComponent, HeaderComponent],
  imports: [
    CommonModule,
    ContentRoutingModule,
    MatIconModule,
    StoreModule.forFeature('selectedPokemonState', selectedPokemonReducer)
  ]
})
export class ContentModule { }
