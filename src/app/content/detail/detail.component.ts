import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { of, Subscription } from 'rxjs';
import { catchError, first, tap } from 'rxjs/operators';
import { AlertMessagesService } from './../../shared/alert-messages/alert-messages.service';
import { ErrorModel } from './../../shared/model/error';
import { Pokemon, PokemonDetail } from './../../shared/model/pokemon';
import { routesApp } from './../../shared/routes/routes';
import { PokemonService } from './../services/pokemon.service';
import { SelectedPokemon } from './../store/actions/selected-pokemon.action';
import { ContentState } from './../store/content.state';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {

  selectedPokemon: Pokemon;
  pokemon: PokemonDetail;

  selectedPokemon$ = new Subscription();

  constructor(
    private store: Store<ContentState>,
    private pokemonService: PokemonService,
    private alertMessagesService: AlertMessagesService,
    private router: Router
  ) { }

  ngOnInit() {
    window.scroll(0, 0);
    this.getPokemonSelected();
    this.getPokemonDetail();
  }

  ngOnDestroy() {
    this.selectedPokemon$.unsubscribe();
    this.store.dispatch(new SelectedPokemon(undefined));
  }

  getPokemonSelected(): void {
    this.selectedPokemon$ = this.store.select(state => state.selectedPokemonState.pokemon).pipe(
      tap((pokemon: Pokemon) => this.selectedPokemon = pokemon),
      tap((pokemon: Pokemon) => {
        if (!pokemon) {
          this.router.navigate([routesApp.content.dashboard]);
        }
      }),
      first((pokemon: Pokemon) => !!pokemon)
    ).subscribe();
  }

  getPokemonDetail(): void {
    if (!this.selectedPokemon) { return; }
    this.pokemonService.searchDetailPokemon(this.selectedPokemon.url, this.selectedPokemon.name).pipe(
      tap((pokemonDetail: PokemonDetail) => this.pokemon = pokemonDetail),
      catchError((error: ErrorModel) => of(this.alertMessagesService.showErrorMessage(error)))
    ).subscribe();
  }

}
