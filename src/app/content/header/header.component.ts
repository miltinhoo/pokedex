import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { tap, first } from 'rxjs/operators';
import { User } from 'src/app/shared/model/user';
import { AppState } from './../../app.reducer';
import { routesApp } from './../../shared/routes/routes';
import { LogoutUser } from './../../shared/store/session/actions/session.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  homeLink = routesApp.content.dashboard;
  userLink = routesApp.content.userData;
  user: User;

  sessionData$ = new Subscription();

  constructor(
    private store: Store<AppState>,
    private router: Router
  ) { }

  ngOnInit() {
    this.getSessionData();
  }

  ngOnDestroy() {
    this.sessionData$.unsubscribe();
  }

  getSessionData(): void {
    this.sessionData$ = this.store.select(state => state.sessionState.userData).pipe(
      tap((user: User) => this.user = user)
    ).subscribe();
  }

  signOut(): void {
    this.store.dispatch(new LogoutUser());
    this.router.navigate([routesApp.session.login]);
  }

}
