import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routerPath } from '../shared/routes/routes';
import { ContentComponent } from './content.component';


const routes: Routes = [
  {
    path: '',
    component: ContentComponent,
    children: [
      {
        path: routerPath.contentPage.dashboard,
        loadChildren: () => import('./dashboard/dashboard.module').then(childModule => childModule.DashboardModule)
      },
      {
        path: routerPath.contentPage.detail,
        loadChildren: () => import('./detail/detail.module').then(childModule => childModule.DetailModule)
      },
      {
        path: routerPath.contentPage.user,
        loadChildren: () =>
          import('./update-session-data/update-session-data.module').then(childModule => childModule.UpdateSessionDataModule)
      },
      {
        path: '',
        redirectTo: routerPath.contentPage.dashboard,
        pathMatch: 'full'
      },
      { path: '**', redirectTo: routerPath.contentPage.dashboard }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
