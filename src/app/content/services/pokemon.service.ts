import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { servicesErrorMessage } from 'src/app/shared/constants/error-text-services';
import { ErrorModel } from './../../shared/model/error';
import { PokemonListSearch, PokemonDetail } from './../../shared/model/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(
    private http: HttpClient
  ) { }

  searchAllPokemon(url: string): Observable<PokemonListSearch | ErrorModel> {
    return this.http.get(url).pipe(
      map(this.pokemonListToModel),
      catchError(this.handleError)
    );
  }

  searchDetailPokemon(url: string, name: string): Observable<PokemonDetail | ErrorModel> {
    return this.http.get(url).pipe(
      map((response: any) => this.podemonDetailToModel(response, name)),
      catchError(this.handleError)
    );
  }

  private pokemonListToModel(response: any): PokemonListSearch {
    if (!response) { return undefined; }
    const {count: pokemonsNumber, next: nextSearch, previous: previousSearch, results} = response;
    return {pokemonsNumber, nextSearch, previousSearch, results};
  }

  private podemonDetailToModel(response: any, name: string): PokemonDetail {
    if (!response) { return undefined; }
    return {
      id: response.id,
      name,
      height: response.height,
      weight: response.weight,
      types: response.types ? response.types.map((type: any) => type.type) : undefined,
      moves: response.moves ? response.moves.map((move: any) => move.move) : undefined
    };
  }

  private handleError(error: any): Observable<ErrorModel> {
    let errorModel: ErrorModel = {
      title: servicesErrorMessage.otherError.title,
      message: servicesErrorMessage.otherError.message,
      status: error.status ? error.status.toString() : undefined
    };
    if (!navigator.onLine) {
      errorModel = {
        title: servicesErrorMessage.offLine.title,
        message: servicesErrorMessage.offLine.message,
        status: error.status ? error.status.toString() : undefined
      };
    }
    return throwError(errorModel);
  }
}
