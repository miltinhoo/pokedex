import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { of, Subscription } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { RegisterService } from 'src/app/authentication/register/register-service/register.service';
import { User } from 'src/app/shared/model/user';
import { AlertMessagesService } from './../../shared/alert-messages/alert-messages.service';
import { messageSaveUserData, titleSaveUserData } from './../../shared/constants/info-text';
import { passwordNotEqualMessage, passwordRepeatMessage } from './../../shared/constants/text-form.constant';
import { FormValidationService } from './../../shared/form-validation/form-validation.service';
import { ErrorModel } from './../../shared/model/error';
import { UpdateUser } from './../../shared/store/session/actions/session.actions';
import { UserDataFormComponent } from './../../shared/user-data-form/user-data-form.component';
import { ContentState } from './../store/content.state';

@Component({
  selector: 'app-update-session-data',
  templateUrl: './update-session-data.component.html',
  styleUrls: ['./update-session-data.component.scss']
})
export class UpdateSessionDataComponent extends UserDataFormComponent implements OnInit, OnDestroy {

  userData$ = new Subscription();
  currentPassword: string;

  passwordRepeat = passwordRepeatMessage;
  passwordNotEqual = passwordNotEqualMessage;

  constructor(
    protected fb: FormBuilder,
    protected formValidationService: FormValidationService,
    protected alertMessagesService: AlertMessagesService,
    protected registerService: RegisterService,
    private store: Store<ContentState>
  ) {
    super(fb, formValidationService, alertMessagesService, registerService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.getUserData();
    this.listenCurrentPassword();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.userData$.unsubscribe();
  }

  createForm(): void {
    this.form = this.fb.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required, this.formValidationService.validateEmail()]],
      currentPassword: ['', [Validators.required]],
      password: [''],
      passwordRepeat: ['']
    });
  }

  getUserData(): void {
    this.userData$ = this.store.select(state => state.sessionState.userData).pipe(
      tap(this.setInitData.bind(this))
    ).subscribe();
  }

  setInitData(userData: User): void {
    if (!this.form || !userData) { return; }
    this.form.controls.fullName.setValue(userData.name);
    this.form.controls.email.setValue(userData.mail);
    this.currentPassword = userData.password;
  }

  listenCurrentPassword(): void {
    this.form.get('password').valueChanges.pipe(
      tap(() => this.form.controls.currentPassword.updateValueAndValidity())
    ).subscribe();
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.registerSubscription$ = this.registerService.updateUser(
        this.form.controls.fullName.value,
        this.form.controls.email.value,
        this.form.controls.password.value
      ).pipe(
        tap((user: User) => this.store.dispatch(new UpdateUser(user))),
        tap(() => this.alertMessagesService.showInfoMessage(titleSaveUserData, messageSaveUserData)),
        catchError((error: ErrorModel) => of(this.alertMessagesService.showErrorMessage(error)))
      ).subscribe();
    }
  }

  addValidationCurrentPassword(): void {
    this.form.controls.currentPassword.setValidators([this.formValidationService.validateCurrentPassword(this.form, this.currentPassword)]);
    this.form.controls.currentPassword.updateValueAndValidity();
  }

}
