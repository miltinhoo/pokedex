import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { UpdateSessionDataRoutingModule } from './update-session-data-routing.module';
import { UpdateSessionDataComponent } from './update-session-data.component';

@NgModule({
  declarations: [UpdateSessionDataComponent],
  imports: [
    CommonModule,
    UpdateSessionDataRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class UpdateSessionDataModule { }
