import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSessionDataComponent } from './update-session-data.component';

describe('UpdateSessionDataComponent', () => {
  let component: UpdateSessionDataComponent;
  let fixture: ComponentFixture<UpdateSessionDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateSessionDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSessionDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
