import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard-search',
  templateUrl: './dashboard-search.component.html',
  styleUrls: ['./dashboard-search.component.scss']
})
export class DashboardSearchComponent implements OnInit {

  form: FormGroup;
  @Output() searchName = new EventEmitter<string>();

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.form = this.fb.group({
      searchText: ['']
    });
  }

  onSubmit(): void {
    if (this.form.controls.searchText && this.form.controls.searchText.value
          && typeof this.form.controls.searchText.value === 'string') {
      const name: string = this.form.controls.searchText.value.toLowerCase();
      this.searchName.emit(name);
    }
  }

}
