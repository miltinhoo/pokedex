import { servicesErrorMessage } from './../../../shared/constants/error-text-services';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, filter, tap } from 'rxjs/operators';
import { ContentState } from '../../store/content.state';
import { AlertMessagesService } from './../../../shared/alert-messages/alert-messages.service';
import { ErrorModel } from './../../../shared/model/error';
import { Pokemon, PokemonListSearch, PokemonDetail } from './../../../shared/model/pokemon';
import { routesApp } from './../../../shared/routes/routes';
import { PokemonService } from './../../services/pokemon.service';
import { SelectedPokemon } from './../../store/actions/selected-pokemon.action';

@Component({
  selector: 'app-dashboard-content',
  templateUrl: './dashboard-content.component.html',
  styleUrls: ['./dashboard-content.component.scss']
})
export class DashboardContentComponent implements OnInit, OnChanges {

  @Input() searchName: string;

  pokemons: Pokemon[] = [];
  pokemonListSearch: PokemonListSearch;
  urlByDefault = 'https://pokeapi.co/api/v2/pokemon/?limit=50';
  urlByName = `https://pokeapi.co/api/v2/pokemon/name`;

  constructor(
    private alertMessagesService: AlertMessagesService,
    private pokemonService: PokemonService,
    private router: Router,
    private store: Store<ContentState>
  ) { }

  ngOnChanges() {
    this.searchByName(this.searchName);
  }

  ngOnInit() {
    this.searchPokemons();
  }

  trackByFunction(index: number, pokemon: Pokemon): string {
    if (!pokemon) { return undefined; }
    return `${pokemon.url}`;
  }

  searchPokemons(url = this.urlByDefault): void {
    this.pokemonService.searchAllPokemon(url).pipe(
      filter((pokemonListSearch: PokemonListSearch) => !!pokemonListSearch),
      tap((pokemonListSearch: PokemonListSearch) => this.pokemonListSearch = pokemonListSearch),
      tap((pokemonListSearch: PokemonListSearch) => {
        if (pokemonListSearch.results && pokemonListSearch.results.length > 0) {
          this.pokemons.push.apply(this.pokemons, pokemonListSearch.results);
        }
      }),
      catchError((error: ErrorModel) => of(this.alertMessagesService.showErrorMessage(error)))
    ).subscribe();
  }

  verPokemon(pokemon: Pokemon): void {
    this.store.dispatch(new SelectedPokemon(pokemon));
    this.router.navigate([routesApp.content.detail]);
  }

  searchByName(name: string): void {
    if (!name) { return; }
    const url = this.urlByName.replace('name', name);
    this.pokemonService.searchDetailPokemon(url, name).pipe(
      tap((pokemonDetail: PokemonDetail) => {
        if (pokemonDetail) {
          this.pokemonListSearch = undefined;
          this.pokemons = [{ name: pokemonDetail.name, url}];
        }
      }),
      catchError((error: ErrorModel) => {
        if (error.status === '404') {
          error.message = servicesErrorMessage.notFound.pokemonSearch.message;
          error.title = servicesErrorMessage.notFound.pokemonSearch.title;
          return of(this.alertMessagesService.showInfoMessage(error.title, error.message));
        }
        return of(this.alertMessagesService.showErrorMessage(error));
      })
    ).subscribe();
  }

  verMasPokemon(): void {
    this.searchPokemons(this.pokemonListSearch.nextSearch);
  }

}
