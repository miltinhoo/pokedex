import { ActionReducerMap } from '@ngrx/store';
import { sessionReducer } from 'src/app/shared/store/session/reducers/session.reducer';
import { AppState } from './../../app.reducer';
import { selectedPokemonReducer, SelectedPokemonState } from './reducers/selected-pokemon.reducer';

export interface ContentState extends AppState {
    selectedPokemonState: SelectedPokemonState;
}

export const contentReducers: ActionReducerMap<ContentState> = {
    sessionState: sessionReducer,
    selectedPokemonState: selectedPokemonReducer
};
