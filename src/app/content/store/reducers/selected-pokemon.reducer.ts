import { Pokemon } from '../../../shared/model/pokemon';
import { SelectedPokemonActions, SELECTED_POKEMON } from '../actions/selected-pokemon.action';

export interface SelectedPokemonState {
    pokemon: Pokemon;
}

export const selectedPokemonInitState: SelectedPokemonState = {
    pokemon: undefined
};

export function selectedPokemonReducer(state = selectedPokemonInitState, action: SelectedPokemonActions): SelectedPokemonState {
    switch (action.type) {
        case SELECTED_POKEMON:
            return {
                ...state,
                pokemon: action.pokemon
            };
        default:
            return state;
    }
}
