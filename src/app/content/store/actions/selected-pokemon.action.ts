import { Action } from '@ngrx/store';
import { Pokemon } from '../../../shared/model/pokemon';

export const SELECTED_POKEMON = '[Pokemon] selected pokemon';

export class SelectedPokemon implements Action {
    readonly type = SELECTED_POKEMON;
    constructor(public pokemon: Pokemon) {}
}

export type SelectedPokemonActions = SelectedPokemon;
