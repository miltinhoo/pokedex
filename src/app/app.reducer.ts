import { ActionReducerMap } from '@ngrx/store';
import { sessionReducer, SessionState } from './shared/store/session/reducers/session.reducer';

export interface AppState {
    sessionState: SessionState;
}

export const appReducers: ActionReducerMap<AppState> = {
    sessionState: sessionReducer
};
