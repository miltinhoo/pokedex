import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { ErrorModel } from './../model/error';

@Injectable({
  providedIn: 'root'
})
export class AlertMessagesService {

  constructor() { }

  showErrorMessage(error: ErrorModel): void {
    Swal.fire({
      icon: 'error',
      title: error.title,
      text: error.message
    });
  }

  showInfoMessage(title: string, text: string): void {
    Swal.fire({
      icon: 'info',
      title,
      text
    });
  }

}
