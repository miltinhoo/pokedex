import { User } from '../model/user';
import { ErrorModel } from '../model/error';

export const userLoginMock: User = {
    name: 'ash ketchum',
    mail: 'ash.ketchum@pokemon.com',
    password: '123'
};


export const userLoginFailMock: ErrorModel = {
    title: 'Datos Invalidos',
    message: 'El usuario o contraseña son invalidos',
    status: '424'
};

export const registerUserErrorMock: ErrorModel = {
    title: 'Error al Crear el usuario',
    message: 'No fue posible crear el usuario',
    status: '500'
};
