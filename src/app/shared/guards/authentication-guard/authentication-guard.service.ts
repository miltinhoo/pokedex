import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { SessionState } from '../../store/session/reducers/session.reducer';
import { AppState } from './../../../app.reducer';
import { routesApp } from '../../routes/routes';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardService {

  constructor(
    private store: Store<AppState>,
    public router: Router
  ) { }

  canActivate(): Observable<boolean> {
    return this.store.select(state => state.sessionState)
    .pipe(
      take(1),
      map((sessionState: SessionState) => {
        if (!sessionState.isLogin) {
          this.router.navigate([routesApp.session.login]);
        }
        return sessionState.isLogin;
      })
    );
  }
}
