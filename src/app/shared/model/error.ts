export interface ErrorModel {
    title: string;
    message: string;
    status: string;
}
