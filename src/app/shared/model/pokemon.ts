export interface Pokemon {
    name: string;
    url: string;
}

export interface PokemonListSearch {
    pokemonsNumber: number;
    nextSearch: string;
    previousSearch: string;
    results: Pokemon[];
}


export interface PokemonDetail {
    id: number;
    name: string;
    height: number;
    weight: number;
    types: PokemonType[];
    moves: PokemonMove[];
}

export interface PokemonType {
    name: string;
    url: string;
}

export interface PokemonMove {
    name: string;
    url: string;
}
