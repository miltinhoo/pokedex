export const invalidEmailMessage = `Debe escribir un email válido`;
export const invalidPasswordMessage = `La contraseña debe contener al menos 8 caracteres,
                                        dos letras mayusculas, un número y un caracter especial`;
export const invalidRepeatPasswordMessage = `Las contraseñas no coinciden`;
export const passwordRepeatMessage = `La nueva contraseña no puede ser igual a la contraseña anterior`;
export const passwordNotEqualMessage = `La nueva contraseña es incorrecta`;
