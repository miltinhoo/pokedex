export const servicesErrorMessage = {
    offLine: {
        title: 'Error en la consulta',
        message: `No fue posible realizar la consulta porque su dispositivo no tiene internet.
                                                        Por favor conectese a una red con internet`
    },
    notFound: {
        pokemonSearch: {
            title: 'Datos no encontrados',
            message: 'No se encontró un pokemon para la busqueda realizada'
        }
    },
    otherError: {
        title: 'Error en la consulta',
        message: `Se presentaron errores técnicos, por favor intenta más tarde`
    }
};
