export function validateEmailRegex(email: string): boolean {
    // tslint:disable-next-line:max-line-length
    const regex: RegExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return email && String(email).match(regex) ? true : false;
}

export function validateNewPassword(password: string): boolean {
    const regex: RegExp = new RegExp(/^(?=(?:.*[A-Z]){2})(?=.*\d)(?=.*[!-/|:-@|[-`|{-~|Ç-■])[!-~\dÇ-■]{8,}$/);
    return password && String(password).match(regex) ? true : false;
}
