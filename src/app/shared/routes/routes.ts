export const routerPath = {
    session: {
        authentication: 'authentication',
        login: 'login',
        register: 'register'
    },
    contentPage: {
        content: 'content',
        dashboard: 'dashboard',
        detail: 'detail',
        user: 'user'
    }
};

export const routesApp = {
    session: {
        login: `/${routerPath.session.authentication}/${routerPath.session.login}`,
        register: `/${routerPath.session.authentication}/${routerPath.session.register}`
    },
    content: {
        dashboard: `/${routerPath.contentPage.content}/${routerPath.contentPage.dashboard}`,
        detail: `/${routerPath.contentPage.content}/${routerPath.contentPage.detail}`,
        userData: `/${routerPath.contentPage.content}/${routerPath.contentPage.user}`
    }
};

