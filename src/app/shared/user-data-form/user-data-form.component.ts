import { OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, tap } from 'rxjs/operators';
import { AlertMessagesService } from '../alert-messages/alert-messages.service';
import { RegisterService } from './../../authentication/register/register-service/register.service';
import { invalidEmailMessage, invalidPasswordMessage, invalidRepeatPasswordMessage } from './../constants/text-form.constant';
import { FormValidationService } from './../form-validation/form-validation.service';

export abstract class UserDataFormComponent implements OnInit, OnDestroy {

  form: FormGroup;

  registerSubscription$ = new Subscription();

  invalidEmail = invalidEmailMessage;
  invalidPassword = invalidPasswordMessage;
  invalidRepeatPassword = invalidRepeatPasswordMessage;

  constructor(
    protected fb: FormBuilder,
    protected formValidationService: FormValidationService,
    protected alertMessagesService: AlertMessagesService,
    protected registerService: RegisterService
  ) { }

  ngOnInit() {
    this.createForm();
    this.listenPassword();
  }

  ngOnDestroy() {
    this.registerSubscription$.unsubscribe();
  }

  abstract createForm();
  abstract onSubmit();

  listenPassword(): void {
    this.form.get('password').valueChanges.pipe(
      distinctUntilChanged(),
      tap(() => this.form.controls.passwordRepeat.updateValueAndValidity())
    ).subscribe();
  }

  addValidationEmail(): void {
    this.form.controls.email.setValidators([this.formValidationService.validateEmail()]);
    this.form.controls.email.updateValueAndValidity();
  }

  addValidationPassword(): void {
    this.form.controls.password.setValidators([this.formValidationService.validateNewPassword()]);
    this.form.controls.password.updateValueAndValidity();
  }

  addValidationReapeatPassword(): void {
    this.form.controls.passwordRepeat.setValidators([this.formValidationService.validateRepeatPassword(this.form, 'password')]);
    this.form.controls.passwordRepeat.updateValueAndValidity();
  }

}
