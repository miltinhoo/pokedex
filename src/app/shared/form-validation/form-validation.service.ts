import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn, FormGroup } from '@angular/forms';
import { validateEmailRegex, validateNewPassword } from '../regex/regex';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor() { }

  validateEmail(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const email: string = control.value;
      if (!validateEmailRegex(email)) {
        return { invalidDate: true, email: true };
    }
      return null;
    };
  }

  validateNewPassword(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const password: string = control.value;
      if (!validateNewPassword(password)) {
        return { invalidDate: true, password: true };
    }
      return null;
    };
  }

  validateRepeatPassword(form: FormGroup, passwordField: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const passwordRepeat = control.value;
      const password: string = form.controls && form.controls[`${passwordField}`] ? form.controls[`${passwordField}`].value : undefined;
      if (!passwordRepeat || password !== passwordRepeat) {
        return { invalidDate: true, passwordRepeat: true };
    }
      return null;
    };
  }

  validateCurrentPassword(form: FormGroup, oldPassword: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const currentPassword = control.value;
      const password: string = form.controls && form.controls.password ? form.controls.password.value : undefined;
      if (oldPassword !== currentPassword) {
        return { invalidDate: true, passwordNotEqual: true };
      }
      if (password === currentPassword) {
        return { invalidDate: true, passwordRepeat: true };
      }
      return null;
    };
  }

}
