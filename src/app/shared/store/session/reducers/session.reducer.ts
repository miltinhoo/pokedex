import { User } from 'src/app/shared/model/user';
import { LOGIN_USER_SUCCESS, SessionActions, LOGIN_USER_FAIL, UPDATE_USER } from '../actions/session.actions';
import { ErrorModel } from '../../../model/error';

export interface SessionState {
    isLogin: boolean;
    userData: User;
    error: ErrorModel;
}

export const sessionInitState: SessionState = {
    isLogin: false,
    userData: undefined,
    error: undefined
};

export function sessionReducer(state = sessionInitState, action: SessionActions): SessionState {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                userData: action.user,
                error: undefined,
                isLogin: true
            };
        case LOGIN_USER_FAIL:
            return {
                ...state,
                userData: undefined,
                error: action.error,
                isLogin: false
            };
        case UPDATE_USER:
            return {
                ...state,
                userData: action.user
            };
        default:
            return state;
    }
}
