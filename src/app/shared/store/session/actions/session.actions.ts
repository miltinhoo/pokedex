import { Action } from '@ngrx/store';
import { User } from 'src/app/shared/model/user';
import { ErrorModel } from 'src/app/shared/model/error';

export const LOGIN_USER = '[Authentication] Login';
export const LOGIN_USER_SUCCESS = '[Authentication] Login has been completed';
export const LOGIN_USER_FAIL = '[Authentication] Login with errors';
export const LOGOUT_USER = '[Authentication] Logout';
export const UPDATE_USER = '[Authentication] Update user data';

export class LoginUser implements Action {
    readonly type = LOGIN_USER;
    constructor(public email: string, public password: string) {}
}

export class LoginUserSuccess implements Action {
    readonly type = LOGIN_USER_SUCCESS;
    constructor(public user: User) {}
}

export class LoginUserFail implements Action {
    readonly type = LOGIN_USER_FAIL;
    constructor(public error: ErrorModel) {}
}

export class LogoutUser implements Action {
    readonly type = LOGOUT_USER;
}

export class UpdateUser implements Action {
    readonly type = UPDATE_USER;
    constructor(public user: User) {}
}

export type SessionActions = LoginUser | LoginUserSuccess | LoginUserFail | LogoutUser | UpdateUser;
