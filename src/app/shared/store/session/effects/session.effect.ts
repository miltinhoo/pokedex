import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ErrorModel } from 'src/app/shared/model/error';
import { User } from 'src/app/shared/model/user';
import { LoginUserFail, LoginUserSuccess, LOGIN_USER } from '../actions/session.actions';
import { LoginService } from '../../../../authentication/login/login-service/login.service';

@Injectable()
export class SessionEffects {
    constructor(
        private actions$: Actions,
        private loginService: LoginService
    ) { }

    @Effect()
    loginUserEffect$ = this.actions$.pipe(
        ofType(LOGIN_USER),
        switchMap((action: any) => {
            const email: string = action.email;
            const password: string = action.password;

            return this.loginService.loginUser(email, password)
                .pipe(
                    map((user: User) => {
                        return new LoginUserSuccess(user);
                    }),
                    catchError((error: ErrorModel) => of(new LoginUserFail(error)))
                );
        })
    );
}
