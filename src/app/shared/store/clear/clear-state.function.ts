import { sessionInitState } from '../session/reducers/session.reducer';
import { LOGOUT_USER } from '../session/actions/session.actions';


export function clearState(reducer) {
    return (state, action) => {
        if (action.type === LOGOUT_USER) {
            return reducer({
                ...state,
                sessionState: { ...sessionInitState }
            }, action);
        }

        return reducer(state, action);
    };
}
